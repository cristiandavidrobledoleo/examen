const express = require('express')
const router = express.Router();
const items = require('../controller/item')

const cors = require('cors')

router.get('/items', cors(), items.getItems);

router.get('/item/:id', cors(), items.getItem);

router.post('/item', cors(), items.addItem);

router.put('/item/:id', cors(), items.updateItem);

router.delete('/item/:id', cors(), items.deleteItem);


module.exports = router