const mongoose = require('mongoose');
require('dotenv').config()

const connectDB = async () => {
    await mongoose.connect(process.env.MONGO_DB_URI)
    console.log("Conexion con Mongo exitosa")
}

module.exports = connectDB