const connectBD = require('../config/config');
const Item = require('../models/item')
connectBD()

const getItems = async (req, res) => {
    try {
        const items = await Item.find();
        res.json(items);
    } catch (error) {
        res.status(404).json({"mensaje": error.message})
    }
}

const getItem = async (req, res) => {
    try {
        const id = req.params.id;
        const item = await Item.findById(id)
        res.json(item);
    } catch (error) {
        res.status(404).json({"mensaje": error.message})
    }
}

const addItem = async (req, res) => { 
    try {
        const { descripcion, nombre } = req.body

        const item = new Item({
            descripcion,
            nombre
        })

        await item.save();
        res.json({"mensaje":"Item añadido con exito"});
    } catch (error) {
        console.log(error)
        res.status(404).json({"mensaje": error.message})
    }
}

const updateItem = async (req, res) => {
    try {
        const id = req.params.id;
        
        const {descripcion, nombre} = req.body

        const item = await Item.findById(id)

        item.nombre = nombre
        item.descripcion = descripcion

        await item.save()
        res.json({"mensaje": "Item actualizado con exito"})
        
    } catch (error) {
        res.status(404).json({"mensaje": error.message})
    }
}

const deleteItem = async (req, res) => {
    try {
        const id = req.params.id;

        const item = await Item.findById(id)

        await item.deleteOne()

        res.json({ "mensaje": "Item eliminado con exito"})
    } catch (error) {
        res.status(404).json({"mensaje": error.message})
    }
}
module.exports = {
    getItems: getItems,
    getItem: getItem,
    addItem: addItem,
    updateItem: updateItem,
    deleteItem: deleteItem
}