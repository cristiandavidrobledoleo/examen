const express = require('express')
const app = express();
require('dotenv').config();
const cors = require('cors');
const router = require('./router/routes');

app.use(cors())
app.use(express.json())
app.use(express.urlencoded({ extended: false }))

app.use('/api', router)

const port = process.env.PORT || 5000;

app.listen(port, () => {
    console.log('listening on port ' + port)
})
