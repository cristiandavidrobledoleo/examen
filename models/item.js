const mongoose = require('mongoose');

const itemSchema = new mongoose.Schema({
    descripcion: {
        type: String,
        required: true
    },
    nombre: {
        type: String,
        required: true
    }
})

const item = mongoose.model('Item', itemSchema)

module.exports = item